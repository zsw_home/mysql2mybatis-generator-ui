# mysql2mybatis-generator-ui

#### 介绍
mybatis 逆向生成 页面操作，不太喜欢不太喜欢mybatis的一些插件插件，喜欢sql来的直观

#### 软件架构
软件架构说明
基于springboot+mybatis


#### 安装教程

第一步：引入pom
```xml
<!-- https://mvnrepository.com/artifact/cn.zhangsw/mysql2mybatis-generator-ui -->
<dependency>
    <groupId>cn.zhangsw</groupId>
    <artifactId>mysql2mybatis-generator-ui</artifactId>
    <version>1.1-release</version>
</dependency>

```

第二步：新建一个class，放入下面代码，设置参数，执行。
```java
  public static void main(String[] args) {
        GeneratorConfig generatorConfig = new GeneratorConfig();
        generatorConfig.setDriverClassName("com.mysql.jdbc.Driver");
        generatorConfig.setUrl("jdbc:mysql://127.0.0.1:3306");
        //SchemaName属性不设置的话，可以将数据库名加在url上
        generatorConfig.setSchemaName("springbootv2");
      ￿￿  generatorConfig.setUsername("root");
        generatorConfig.setPassword("1111");
        generatorConfig.setPort(8085);
        Mysql2MybatisUITool.run(generatorConfig);

    }
```


#### 使用说明
第一步    ![数据库表展示](https://gitee.com/zsw_home/mysql2mybatis-generator-ui/raw/master/img1.png)

第二步    ![全局配置说明](https://gitee.com/zsw_home/mysql2mybatis-generator-ui/raw/master/img2.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)