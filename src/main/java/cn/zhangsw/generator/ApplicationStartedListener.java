package cn.zhangsw.generator;


import cn.zhangsw.generator.util.LocalBrowserUtil;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

public class ApplicationStartedListener implements ApplicationListener<ApplicationStartedEvent> {
    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        int port = Mysql2MybatisUITool.generatorConfig.getPort();
        LocalBrowserUtil.browse("http://127.0.0.1:" + port);
    }
}
