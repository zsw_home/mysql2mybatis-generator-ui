package cn.zhangsw.generator;

import cn.hutool.core.util.StrUtil;
import cn.zhangsw.generator.config.GeneratorConfig;
import cn.zhangsw.generator.util.StringUtils;
import com.zaxxer.hikari.HikariDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;


/**
 * 项目启动方法
 *
 * @author fuce
 */
@SpringBootConfiguration
@MapperScan(basePackages = "cn.zhangsw.generator.dao")
@Import({ ServletWebServerFactoryAutoConfiguration.class})
@ComponentScan("cn.zhangsw.generator")
@EnableAutoConfiguration
public class Mysql2MybatisUITool {


    public static GeneratorConfig generatorConfig;

    static {

    }


    public static void run(GeneratorConfig generatorConfig) {


        String tableName = StringUtils.findDataBaseNameByUrl(generatorConfig.getUrl());

        String schemaName = generatorConfig.getSchemaName();


        if (StringUtils.isEmpty(tableName)) {

            if (StringUtils.isEmpty(schemaName)) {
                throw new IllegalArgumentException("jdbcurl格式错误，无法获取数据库");

            } else {

                generatorConfig.setUrl(generatorConfig.getUrl() + "/" + schemaName);
            }


        } else if (StringUtils.isEmpty(schemaName)) {
            generatorConfig.setSchemaName(tableName);
        }


        Mysql2MybatisUITool.generatorConfig = generatorConfig;


        //异步调用

        SpringApplicationBuilder sources = new SpringApplicationBuilder()
                .sources(Mysql2MybatisUITool.class).profiles("mybatis");
        sources.application().addListeners(new ApplicationStartedListener());
        sources.run(new String[0]);
    }


    @Bean
    public GeneratorConfig getDataSource() {
            if(generatorConfig == null ){
                generatorConfig = new GeneratorConfig();
                generatorConfig.setUrl("jdbcurl");
            }
        return generatorConfig;
    }

    @Bean
    public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> containerConfig(GeneratorConfig generatorConfig) {
        return factory -> {

            if (generatorConfig.getPort() == 0) {
                factory.setPort(8080);
            } else {

                factory.setPort(generatorConfig.getPort());
            }


            factory.setContextPath("");
        };
    }


    @Bean
    public DataSource dataSource(GeneratorConfig config) {
        if (StrUtil.isBlank(config.getUrl())) {
            throw new IllegalArgumentException("必须指定jdbcUrl用于创建数据源");
        }
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl(config.getUrl());
        ds.setUsername(config.getUsername());
        ds.setPassword(config.getPassword());
        ds.setDriverClassName(config.getDriverClassName());
        return ds;
    }


}
