package cn.zhangsw.generator.config;


import cn.zhangsw.generator.util.StringUtils;

import java.util.List;

/**
 * 自动生成配置文件
 */
public class GlobalConfig {
	/**
     * 表名称集合
     */
    private List<TableConfig> tableNames;


/*            表关系配置
    private RelationConfig relationConfig;*/
    /**
     * 生成代码的父包 如父包是com.fc.test  controller将在com.fc.test.controller下 bean 将在com.fc.test.bean下 ,service,dao同理
     */
    private String parentPack;
    /**
     * 生成代码的项目路径 D:\Users\Administrator\git\SpringBoot_v2
     */
    private String parentPath;

    /**
     * 是否覆盖生成 默认不覆盖
     */
    private Boolean cover=false;

    private Boolean swagger=false;


    /**
     * java在项目中的classpath位置
     */
    private String javaSource="src/main/java";

    /**
     * resource位置
     */
    private String resources="src/main/resources";
    /**
     * xml存放的路径
     */
    private String xmlPath;

    /**
     * bean的包名
     */
    private String beanPackageName;
    /**
     * dao的包名
     */
    private String daoPackageName;
    /**
     * controller的包名
     */
    private String controllerPackageName;
    /**
     * service的包名
     */
    private String servicePackageName;
    /**
     * bean的后缀
     */
    private String beanNameSuffix;
    /**
     * dao的后缀
     */
    private String daoNameSuffix;
    /**
     * controller的后缀
     */
    private String controllerNameSuffix;
    /**
     * service的后缀
     */
    private String serviceNameSuffix;

	/**
	 * 表前缀
	 */
    private String tablePrefix;
    /**
     * 是否启用代码生成器
     */
    private Boolean autoCode=true;



    /**
     * 作者名字
     */
    private String author;




    public Boolean getAutoCode() {
        return autoCode;
    }

    public void setAutoCode(Boolean autoCode) {
        this.autoCode = autoCode;
    }

    public Boolean getSwagger() {
        return swagger;
    }

    public void setSwagger(Boolean swagger) {
        this.swagger = swagger;
    }

    public Boolean getCover() {
        return cover;
    }



    public void setCover(Boolean cover) {
        this.cover = cover;
    }

    public String getXmlPath() {
        return xmlPath;
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }

    public String getJavaSource() {
        return javaSource;
    }

    public void setJavaSource(String javaSource) {
        this.javaSource = javaSource;
    }

    public String getResources() {
        return resources;
    }

    public void setResources(String resources) {
        this.resources = resources;
    }





    public List<TableConfig> getTableNames() {
        return tableNames;
    }

    public void setTableNames(List<TableConfig> tableNames) {
        this.tableNames = tableNames;
    }

    public String getParentPath() {
        return parentPath;
    }
    public String getParentPathResources() {
        return parentPath+ "/" + getResources();
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getParentPack() {
        return parentPack;
    }

    public String getParentPathJavaSource() {
        return parentPath+ "/" + getJavaSource();
    }

    public void setParentPack(String parentPack) {
        this.parentPack = parentPack;
    }

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

    public String getBeanPackageName() {
        return beanPackageName;
    }

    public void setBeanPackageName(String beanPackageName) {
        this.beanPackageName = beanPackageName;
    }

    public String getDaoPackageName() {
        return daoPackageName;
    }

    public void setDaoPackageName(String daoPackageName) {
        this.daoPackageName = daoPackageName;
    }

    public String getControllerPackageName() {
        return controllerPackageName;
    }

    public void setControllerPackageName(String controllerPackageName) {
        this.controllerPackageName = controllerPackageName;
    }

    public String getServicePackageName() {
        return servicePackageName;
    }

    public void setServicePackageName(String servicePackageName) {
        this.servicePackageName = servicePackageName;
    }

    public String getBeanNameSuffix() {
        return beanNameSuffix;
    }

    public void setBeanNameSuffix(String beanNameSuffix) {
        this.beanNameSuffix = beanNameSuffix;
    }

    public String getDaoNameSuffix() {
        return daoNameSuffix;
    }

    public void setDaoNameSuffix(String daoNameSuffix) {
        this.daoNameSuffix = daoNameSuffix;
    }

    public String getControllerNameSuffix() {
        return controllerNameSuffix;
    }

    public void setControllerNameSuffix(String controllerNameSuffix) {
        this.controllerNameSuffix = controllerNameSuffix;
    }

    public String getServiceNameSuffix() {
        return serviceNameSuffix;
    }

    public void setServiceNameSuffix(String serviceNameSuffix) {
        this.serviceNameSuffix = serviceNameSuffix;
    }

    public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}


	public GlobalConfig(List<TableConfig> tableNames, String parentPack, String parentPath, Boolean cover,
						Boolean swagger, String javaSource, String resources, String xmlPath,
                        String beanPackageName, String daoPackageName, String controllerPackageName, String servicePackageName,
                        String beanNameSuffix, String daoNameSuffix, String controllerNameSuffix, String serviceNameSuffix,

                        String tablePrefix, Boolean autoCode, String author) {
		super();
		this.tableNames = tableNames;
		this.parentPack = parentPack;
		this.parentPath = parentPath;
		this.cover = cover;
		this.swagger = swagger;
		this.javaSource = javaSource;
		this.resources = resources;
		this.xmlPath = xmlPath;
        this.beanPackageName = beanPackageName;
        this.daoPackageName = daoPackageName;
        this.controllerPackageName = controllerPackageName;
        this.servicePackageName = servicePackageName;
        this.beanNameSuffix = beanNameSuffix;
        this.daoNameSuffix = daoNameSuffix;
        this.controllerNameSuffix = controllerNameSuffix;
        this.serviceNameSuffix = serviceNameSuffix;
		this.tablePrefix = tablePrefix;
		this.autoCode = autoCode;
        this.author= author;

	}

	public GlobalConfig() {
		super();
	}




}
