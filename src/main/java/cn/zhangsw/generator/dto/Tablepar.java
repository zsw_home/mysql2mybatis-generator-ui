package cn.zhangsw.generator.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * boostrap table post 参数
 *
 * @author fc
 */
@ApiModel("Tablepar")
public class Tablepar implements Serializable {
    @ApiModelProperty(value = "页码")
    private int pageNum;//
    @ApiModelProperty(value = "数量")
    private int pageSize;//
    @ApiModelProperty(value = "排序字段")
    private String orderByColumn;//
    @ApiModelProperty(value = "排序字符 asc desc")
    private String isAsc;//

    public int getPageNum() {
        return pageNum = pageNum == 0 ? 1 : pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum == 0 ? 1 : pageNum;
    }

    public int getPageSize() {
        return pageSize = pageSize == 0 ? 10 : pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize == 0 ? 10 : pageSize;
    }

    public String getOrderByColumn() {
        return orderByColumn;
    }

    public void setOrderByColumn(String orderByColumn) {
        this.orderByColumn = orderByColumn;
    }

    public String getIsAsc() {
        return isAsc;
    }

    public void setIsAsc(String isAsc) {
        this.isAsc = isAsc;
    }

}
