package cn.zhangsw.generator.controller;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import cn.zhangsw.generator.config.AutoCodeConfig;
import cn.zhangsw.generator.config.GeneratorConfig;
import cn.zhangsw.generator.config.GlobalConfig;
import cn.zhangsw.generator.model.BeanColumn;
import cn.zhangsw.generator.model.TsysTables;
import cn.zhangsw.generator.service.GeneratorService;
import cn.zhangsw.generator.util.AutoCodeUtil;
import cn.zhangsw.generator.vo.AjaxResult;
import cn.zhangsw.generator.vo.TitleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 代码自动生成
 */
@Api(value = "代码自动生成")
@Controller
public class AutoCodeController {

	private String prefix = "admin/auto_code";
	@Autowired
	private GeneratorService generatorService;

	@Autowired
	private GeneratorConfig generatorConfig ;

	/**
	 * 代码自动生成展示首页
	 */
	@ApiOperation(value = " 代码自动生成展示首页", notes = " 代码自动生成展示首页")
	@GetMapping("/")
	public String one(ModelMap model) {
		String str = "单表代码生成";
		setTitle(model, new TitleVo("生成", str + "管理", true, "欢迎进入" + str + "页面", true, false));
		List<TsysTables> tables = generatorService.queryList(null);
		model.addAttribute("tables", tables);
		return prefix + "/one";
	}

	/**
	 * 代码自动生成全局配置
	 *
	 */
	@ApiOperation(value = " 代码自动生成全局配置", notes = "代码自动生成全局配置")
	@GetMapping("/global")
	public String global(ModelMap modelMap) {
		String str = "全局配置";
		setTitle(modelMap, new TitleVo("配置", str + "管理", true, "欢迎进入" + str + "页面", true, false));

		modelMap.put("autoConfig", AutoCodeConfig.getGlobalConfig());
		System.out.println(JSONUtil.toJsonStr(AutoCodeConfig.getGlobalConfig()));
		return prefix + "/global";
	}

	/**
	 * 根据表名查询表信息
	 */
	@ApiOperation(value = "根据表名查询表信息", notes = "根据表名查询表信息")
	@PostMapping("/queryTable")
	@ResponseBody
	public AjaxResult queryTable(String tableName) {

		List<TsysTables> list = generatorService.queryList(tableName);
		if (list.size() > 0) {
			return AjaxResult.successData(200, list);
		}
		return AjaxResult.error();
	}

	/**
	 * 根据表查询表字段详情
	 *
	 * @param tableName
	 */
	@ApiOperation(value = "根据表查询表字段详情", notes = "根据表查询表字段详情")
	@PostMapping("/queryTableInfo")
	@ResponseBody
	public AjaxResult queryTableInfo(String tableName) {
		List<BeanColumn> list = generatorService.queryColumns2(tableName);
		System.out.println(JSONUtil.toJsonStr(list));
		if (list.size() > 0) {
			return AjaxResult.successData(200, list);
		}
		return AjaxResult.error();
	}

	/**
	 * 保存配置文件
	 *
	 * @param globalConfig
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 * @author fuce
	 * @Date 2019年8月24日 下午3:25:41
	 */
	@ApiOperation(value = "保存配置文件", notes = "保存配置文件")
	@PostMapping("/addGlobal")
	@ResponseBody
	public AjaxResult addGlobal(GlobalConfig globalConfig, Model model, HttpServletRequest request) throws Exception {
		AutoCodeConfig.setGlobalConfig(globalConfig);
		if (globalConfig != null) {
			return AjaxResult.success();
		} else {
			return AjaxResult.error();
		}
	}

	/**
	 * 保存单表信息
	 *
	 * @param allColumnss  字段列表字符串
	 * @param tableName 表名
	 * @param conditionQueryField  条件查询字段
	 * @param pid 父id
	 * @param sqlcheck 是否录入数据
	 * @param vhtml 生成html
	 * @param vController 生成controller
	 * @param vservice 生成service
	 * @param vMapperORdao 生成mapper or dao
	 * @return
	 * @throws Exception
	 * @author fuce
	 * @Date 2019年8月31日 上午2:49:18
	 */
	@ApiOperation(value = "保存单表信息", notes = "保存单表信息")
	@PostMapping("/saveOne")
	@ResponseBody
	public AjaxResult saveOne(String allColumnss, String tableName, String conditionQueryField,Boolean vController,Boolean vService,Boolean vMapperORdao) throws Exception {
		JSONArray array = JSONUtil.parseArray(allColumnss);
		// 遗留可用前端修改传入的字段等信息（未完善）
		List<BeanColumn> beanColumns2 = JSONUtil.toList(array, BeanColumn.class);
		List<TsysTables> list = generatorService.queryList(tableName);
		if (list.size() > 0) {
			TsysTables tables = list.get(0);
			List<BeanColumn> beanColumns = generatorService.queryColumns2(tableName);
			AutoCodeUtil.autoCodeOneModel(tables, beanColumns, conditionQueryField, vController, vService, vMapperORdao);
		}
		return AjaxResult.success();
	}

	/**
	 * 设置标题通用方法
	 * @param model
	 */
	private void setTitle(ModelMap map, TitleVo titleVo){
		//标题
		map.put("title",titleVo.getTitle());
		map.put("parenttitle",titleVo.getParenttitle());
		//是否打开欢迎语
		map.put("isMsg",titleVo.isMsg());
		//欢迎语
		map.put("msgHTML",titleVo.getMsgHtml());
		//小控件
		map.put("isControl",titleVo.isControl());
		map.put("isribbon", titleVo.isIsribbon());
	}
}
