package cn.zhangsw.generator.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassNameResult
 * @Description
 * @Author
 * @Date2019/10/25 10:04
 * @Version V1.0
 **/
@ApiModel
public class Result<T> implements Serializable {
    @ApiModelProperty(value = "返回状态")
   private int code;
    @ApiModelProperty(value = "返回状态信息")
    private String msg;
    @ApiModelProperty(value = "数据")
    private T data;


    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result() {
    }

    public static Result success() {
        return new Result(200, "成功");
    }


    public static Result success(Object data) {
        return new Result(200, "成功", data);
    }


    public static Result fail() {
        return  new Result(500, "失败");

    }

    public static Result fail(String msg) {
        return  new Result(500, msg);

    }

    public static Result fail(int code, String msg) {
         return new Result(code, msg);
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

